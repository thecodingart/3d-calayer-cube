//
//  ViewController.m
//  CubeTest
//
//  Created by Brandon Levasseur on 5/5/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import "ViewController.h"
@import GLKit;
@import QuartzCore;


#define LIGHT_DIRECTION 0, 1, -0.5
#define AMBIENT_LIGHT 0.5

@interface ViewController ()

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *faces;

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) UIPanGestureRecognizer *rotationGesture;

@property (strong, nonatomic) CALayer *cube1;
@property (strong, nonatomic) CALayer *cube2;

@property (assign, nonatomic) CGPoint firstTouch;

@end

@implementation ViewController

/* midpoint, startpoint, point */
CGFloat CGPointAngleATan2(CGPoint mp, CGPoint sp, CGPoint p)
{
    CGFloat angle = 0;
    CGFloat sAngle = atan2(sp.y-mp.y,sp.x-mp.x);
    CGFloat pAngle = atan2(p.y-mp.y,p.x-mp.x);
    angle = pAngle-sAngle;
    return angle;
}

CGFloat AngleForVector(CGPoint vector)
{
    CGFloat delta = vector.x - vector.y;
    return delta;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.layer.doubleSided = NO;
    CATransform3D perspective = CATransform3DIdentity;
    perspective.m34 = -1.0/500.0;
    self.containerView.layer.sublayerTransform = perspective;
    
    //set up the transform for cube 1 and add it
    CATransform3D clt = CATransform3DIdentity;
    clt = CATransform3DTranslate(clt, -100, 0, 0);
    self.cube1 = [self cubeWithTransform:clt];
    [self.containerView.layer addSublayer:self.cube1];
    
    CATransform3D c2t = CATransform3DIdentity;
    c2t = CATransform3DTranslate(c2t, 100, 0, 0);
    c2t = CATransform3DRotate(c2t, -M_PI_4, 1, 0, 0);
    c2t = CATransform3DRotate(c2t, -M_PI_4, 0, 1, 0);
    self.cube2 = [self cubeWithTransform:c2t];
    [self.containerView.layer addSublayer:self.cube2];
    
    self.rotationGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(gestureDetected:)];
    [self.containerView addGestureRecognizer:self.rotationGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CALayer *)faceWithTranformation:(CATransform3D)transform
{
    CALayer *face = [CALayer layer];
    face.frame = CGRectMake(-50, -50, 100, 100);
    
    CGFloat red = (rand() / (double)INT_MAX);
    CGFloat green = (rand()/(double)INT_MAX);
    CGFloat blue = (rand()/(double)INT_MAX);
    face.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor;
    
    face.transform = transform;
    
    [self applyLightingToFace:face];
    
    return face;
}

- (void)applyLightingToFace:(CALayer *)face
{
    CALayer *layer = [CALayer layer];
    layer.frame = face.bounds;
    [face addSublayer:layer];
    
    //convert the face transform to matrix
    //(GLKMatrix4 has the same structure as CATransform3D)
    CATransform3D transform = face.transform;
    GLKMatrix4 matrix4 = [self matrixFrom3DTransformation:transform];
    GLKMatrix3 matrix3 = GLKMatrix4GetMatrix3(matrix4);
    
    //get face normal
    GLKVector3 normal = GLKVector3Make(0, 0, 1);
    normal = GLKMatrix3MultiplyVector3(matrix3, normal);
    normal = GLKVector3Normalize(normal);
    
    //get dot product with light direction
    GLKVector3 light = GLKVector3Normalize(GLKVector3Make(LIGHT_DIRECTION));
    float dotProduct = GLKVector3DotProduct(light, normal);
    
    //set lighting layer opacity
    CGFloat shadow = 1 + dotProduct - AMBIENT_LIGHT;
    UIColor *color = [UIColor colorWithWhite:0 alpha:shadow];
    layer.backgroundColor = color.CGColor;
}

- (CALayer *)cubeWithTransform:(CATransform3D)transform
{
    
    CATransformLayer *cube = [CATransformLayer layer];
    CATransform3D ct = CATransform3DMakeTranslation(0, 0, 50);
    [cube addSublayer:[self faceWithTranformation:ct]];
    
    ct = CATransform3DMakeTranslation(50, 0, 0);
    ct = CATransform3DRotate(ct, M_PI_2, 0, 1, 0);
    [cube addSublayer:[self faceWithTranformation:ct]];
    
    ct = CATransform3DMakeTranslation(0, -50, 0);
    ct = CATransform3DRotate(ct, -M_PI_2, 1, 0, 0);
    [cube addSublayer:[self faceWithTranformation:ct]];
    
    ct = CATransform3DMakeTranslation(0, 50, 0);
    ct = CATransform3DRotate(ct,- M_PI_2, 1, 0, 0);
    [cube addSublayer:[self faceWithTranformation:ct]];
    
    ct = CATransform3DMakeTranslation(-50, 0, 0);
    ct = CATransform3DRotate(ct, M_PI_2, 0, 1, 0);
    [cube addSublayer:[self faceWithTranformation:ct]];
    
    ct = CATransform3DMakeTranslation(0, 0, -50);
    ct = CATransform3DRotate(ct, M_PI, 0, 1, 0);
    [cube addSublayer:[self faceWithTranformation:ct]];
    
    CGSize containerSize = self.containerView.bounds.size;
    cube.position = CGPointMake(containerSize.width/2, containerSize.height/2);
    
    cube.transform = transform;
    return cube;
}

- (void)gestureDetected:(UIGestureRecognizer *)gesture
{
    if ([gesture isKindOfClass:[UIPanGestureRecognizer class]]) {
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer *)gesture;
        
        switch (panGesture.state) {
            case UIGestureRecognizerStateEnded:
                self.firstTouch = [gesture locationInView:self.containerView];
                break;
                case UIGestureRecognizerStateChanged:
            {
                CGPoint point = [panGesture locationInView:self.containerView];
                CGFloat rotation = AngleForVector(CGPointMake(self.firstTouch.x, point.x));
                self.cube1.transform = CATransform3DRotate(self.cube1.transform, rotation, 0, 1, 0 );
                self.cube2.transform = CATransform3DRotate(self.cube2.transform, rotation, 0, 1, 0 );
            }
                break;
                
            default:
                break;
        }
        
        
    }
}


#pragma mark - Helper methods

- (GLKMatrix4)matrixFrom3DTransformation:(CATransform3D)transform
{
    GLKMatrix4 matrix = GLKMatrix4Make(transform.m11, transform.m12, transform.m13, transform.m14,
                                       transform.m21, transform.m22, transform.m23, transform.m24,
                                       transform.m31, transform.m32, transform.m33, transform.m34,
                                       transform.m41, transform.m42, transform.m43, transform.m44);
    
    return matrix;
}




@end
